FROM openjdk:17

RUN mkdir -p /app

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /app/app.jar

WORKDIR /app 


CMD ["java", "-jar", "app.jar"]