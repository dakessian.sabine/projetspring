package com.formation.demospring;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoSpringApplicationTests {

	@Test
	void contextLoads() {
		System.out.println( "Test is OK !" );
	}

}
