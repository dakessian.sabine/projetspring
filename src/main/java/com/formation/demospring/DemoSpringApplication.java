package com.formation.demospring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringApplication {

	private static final String constant = "constant";
	public static void main(String[] args) {
		SpringApplication.run(DemoSpringApplication.class, args);
	}

}
